var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log("Proyecto polymer con wrapper de node.js escuchando en: ", port);

app.get('/', function(req, res) {
    res.sendFile("index.html", {root:'.'});
})